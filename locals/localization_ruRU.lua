-- ********************************************************
-- **               Memoria - ruRU Local                 **
-- **           <http://nanaki.affenfelsen.de>           **
-- ********************************************************
--
-- This localization is written by:
--  Lisichk0
--

-- Check for addon table
if (not Memoria) then Memoria = {}; end
if GetLocale() ~= "ruRU" then return end

-- Localization
Memoria.L["arena endings"] = "сражение на арене завершено"
Memoria.L["battleground endings"] = "сражение на поле боя завершено"
--[[Translation missing --]]
--[[ Memoria.L["bosskills"] = "boss kills"--]] 
--[[Translation missing --]]
--[[ Memoria.L["challenge instance endings"] = "mythic+ instance endings"--]] 
Memoria.L["exalted only"] = "только \"Превознесение\""
Memoria.L["level up"] = "уровень повысился"
Memoria.L["new achievement"] = "новое достижение"
Memoria.L["new reputation level"] = "новый уровень репутации"
--[[Translation missing --]]
--[[ Memoria.L["only after first kill"] = "only once per difficulty"--]] 
Memoria.L["Take screenshot on"] = "Делать снимок экрана, когда"
Memoria.L["wins only"] = "только при победе"

