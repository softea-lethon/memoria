-- ********************************************************
-- **               Memoria - koKR Local                 **
-- **           <http://nanaki.affenfelsen.de>           **
-- ********************************************************
--
-- This localization is written by:
--  maknae 
--

-- Check for addon table
if (not Memoria) then Memoria = {}; end
if GetLocale() ~= "koKR" then return end

-- Localization
Memoria.L["arena endings"] = "투기장 끝날때"
Memoria.L["battleground endings"] = "전장 끝날때"
Memoria.L["bosskills"] = "보스 킬할시"
Memoria.L["challenge instance endings"] = "쐐기 끝날때"
Memoria.L["exalted only"] = "확고한 동맹시"
Memoria.L["level up"] = "레벨 업"
Memoria.L["new achievement"] = "업적"
Memoria.L["new reputation level"] = "평판 레벨 업"
Memoria.L["only after first kill"] = "처음 처치시만"
Memoria.L["Take screenshot on"] = "스크린 샷 찍기"
Memoria.L["wins only"] = "승리시"

