-- ********************************************************
-- **               Memoria - zhTW Local                 **
-- **           <http://nanaki.affenfelsen.de>           **
-- ********************************************************
--
-- This localization is written by:
--  <nobody>
--

-- Check for addon table
if (not Memoria) then Memoria = {}; end
if GetLocale() ~= "zhTW" then return end

-- Localization
Memoria.L = {
  ["Take screenshot on"] = "Take screenshot on",
  ["arena endings"] = "arena endings",
  ["battleground endings"] = "battleground endings",
  ["bosskills"] = "boss kills",
  ["challenge instance endings"] = "mythic+ instance endings",
  ["days"] = " days, ",
  ["exalted only"] = "exalted only",
  ["hours"] = " hours, ",
  ["level up"] = "level up",
  ["minutes"] = " minutes, ",
  ["new achievement"] = "new achievement",
  ["new reputation level"] = "new reputation level",
  ["only after first kill"] = "only once per difficulty",
  ["seconds"] = " seconds",
  ["show played"] = "show /played in chat",
  ["show previous"] = "show previous level time to complete",
  ["time played"] = "Time played at level ",
  ["wins only"] = "wins only",
}
