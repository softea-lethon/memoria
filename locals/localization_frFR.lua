-- ********************************************************
-- **               Memoria - frFR Local                 **
-- **           <http://nanaki.affenfelsen.de>           **
-- ********************************************************
--
-- This localization is written by:
--  Deepking
--

-- Check for addon table
if (not Memoria) then Memoria = {}; end
if GetLocale() ~= "frFR" then return end

-- Localization
Memoria.L["arena endings"] = "fin d'un match d'arène"
Memoria.L["battleground endings"] = "fin d'un champs de bataille"
--[[Translation missing --]]
--[[ Memoria.L["bosskills"] = "boss kills"--]] 
--[[Translation missing --]]
--[[ Memoria.L["challenge instance endings"] = "mythic+ instance endings"--]] 
Memoria.L["exalted only"] = "exalté seulement"
Memoria.L["level up"] = "up d'un niveau"
Memoria.L["new achievement"] = "nouveau haut fait"
Memoria.L["new reputation level"] = "nouveau niveau de réputation"
--[[Translation missing --]]
--[[ Memoria.L["only after first kill"] = "only once per difficulty"--]] 
Memoria.L["Take screenshot on"] = "Faire une capture d'écran quand"
Memoria.L["wins only"] = "victoires seulement"

