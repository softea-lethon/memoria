-- ********************************************************
-- **               Memoria - itIT Local                 **
-- **           <http://nanaki.affenfelsen.de>           **
-- ********************************************************
--
-- This localization is written by:
--  Deepking
--

-- Check for addon table
if (not Memoria) then Memoria = {}; end
if GetLocale() ~= "itIT" then return end

-- Localization
Memoria.L["arena endings"] = "schermata finale arena"
Memoria.L["battleground endings"] = "riassunto finale del battleground"
--[[Translation missing --]]
--[[ Memoria.L["bosskills"] = "boss kills"--]] 
--[[Translation missing --]]
--[[ Memoria.L["challenge instance endings"] = "mythic+ instance endings"--]] 
Memoria.L["exalted only"] = "solo Esaltato"
Memoria.L["level up"] = "nuovo livello"
Memoria.L["new achievement"] = "nuova impresa/achievement"
Memoria.L["new reputation level"] = "nuovo livello di reputazione"
--[[Translation missing --]]
--[[ Memoria.L["only after first kill"] = "only once per difficulty"--]] 
Memoria.L["Take screenshot on"] = "Fai uno screenshot su:"
Memoria.L["wins only"] = "solo vittoria"

