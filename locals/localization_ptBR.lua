-- ********************************************************
-- **               Memoria - ptBR Local                 **
-- **           <http://nanaki.affenfelsen.de>           **
-- ********************************************************
--
-- This localization is written by:
--  Deepking
--

-- Check for addon table
if (not Memoria) then Memoria = {}; end
if GetLocale() ~= "ptBR" then return end

-- Localization
--[[Translation missing --]]
--[[ Memoria.L["arena endings"] = "arena endings"--]] 
--[[Translation missing --]]
--[[ Memoria.L["battleground endings"] = "battleground endings"--]] 
--[[Translation missing --]]
--[[ Memoria.L["bosskills"] = "boss kills"--]] 
--[[Translation missing --]]
--[[ Memoria.L["challenge instance endings"] = "mythic+ instance endings"--]] 
--[[Translation missing --]]
--[[ Memoria.L["exalted only"] = "exalted only"--]] 
--[[Translation missing --]]
--[[ Memoria.L["level up"] = "level up"--]] 
--[[Translation missing --]]
--[[ Memoria.L["new achievement"] = "new achievement"--]] 
--[[Translation missing --]]
--[[ Memoria.L["new reputation level"] = "new reputation level"--]] 
--[[Translation missing --]]
--[[ Memoria.L["only after first kill"] = "only once per difficulty"--]] 
--[[Translation missing --]]
--[[ Memoria.L["Take screenshot on"] = "Take screenshot on"--]] 
--[[Translation missing --]]
--[[ Memoria.L["wins only"] = "wins only"--]] 

