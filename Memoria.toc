## Interface: 80100
## Title: Memoria
## Version: 8.1
## Notes: Memoria automatically screenshots various milestones of your characters.
## Author: Mîzukichan@EU-Antonidas
## LoadOnDemand: 0
## DefaultState: enabled
## SavedVariables: Memoria_Options
## SavedVariablesPerCharacter: Memoria_CharBossKillDB, Memoria_CharLevelTimes
## X-Website: http://cosmocanyon.de

embeds.xml
locals.xml
Options.xml
Options.lua
Memoria.lua
